﻿using System;
using System.Collections.Generic;
using System.Windows;


namespace _26._3
{


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Land France;
        Land Germany;
        public MainWindow()
        {
            InitializeComponent();
        }
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {


            List<Land> objects = new List<Land>();

            Monarchie monarchie = new Monarchie(naam: (txtLand.Text), hoofdstat: (txtHoofdstad.Text), koning: (txtStaatshoofd.Text));
            Republiek republiek = new Republiek(naam: (txtLand.Text), hoofstad: (txtHoofdstad.Text), president: (txtStaatshoofd.Text));
            Land land = new Land(naam: (txtLand.Text), hoofdstad: (txtHoofdstad.Text));


            objects.Add(monarchie);
            objects.Add(republiek);
            objects.Add(land);

            if (rdMonarchie.IsChecked == true)
            {



                txtresult.Text += Environment.NewLine + monarchie.Naam + " " + monarchie.Hoofdstat + " " + monarchie.Koning;
            }
            else if (rdRepubliek.IsChecked == true)
            {


                txtresult.Text += Environment.NewLine + republiek.Naam + " " + republiek.Hoofdstat + " " + republiek.President;


            }
            else if (rdOverige.IsChecked == true)
            {


                txtresult.Text += Environment.NewLine + land.Naam + " " + land.Hoofdstat + " ";
            }
        }

        private void rdRepubliek_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void rdMonarchie_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void rdOverige_Checked(object sender, RoutedEventArgs e)
        {

        }
    }
}
