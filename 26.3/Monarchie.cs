﻿namespace _26._3
{
    class Monarchie : Land
    {
        private string _koning;

        public string Koning { get; set; }


        public Monarchie(string naam, string hoofdstat, string koning) : base(naam, hoofdstat)
        {

            Koning = koning;
        }


    }
}
