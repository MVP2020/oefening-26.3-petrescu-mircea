﻿namespace _26._3
{
    class Republiek : Land
    {
        private string _president;
        public string President { get; set; }

        public Republiek(string naam, string hoofstad, string president) : base(naam, hoofstad)
        {
            President = president;
        }

        public override string ToString()
        {
            return base.ToString();
        }

    }
}
